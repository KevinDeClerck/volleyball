/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.maven.volleyballKlassement.mapper;

import com.realdolmen.maven.volleyballKlassement.domain.Player;
import com.realdolmen.maven.volleyballKlassement.dto.PlayerDTO;
import java.util.function.Function;

/**
 *
 * @author KDLBL62
 */
public class PlayerMapper implements Function<Player,PlayerDTO> {

    @Override
    public PlayerDTO apply(Player player) {
        PlayerDTO dto = new PlayerDTO(player.getId(),player.getFirstName(),player.getLastName(),player.getBirthDate());
        return dto;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.maven.volleyballKlassement.service;

import com.realdolmen.maven.volleyballKlassement.domain.Player;
import com.realdolmen.maven.volleyballKlassement.repository.PlayerRepository;
import java.util.List;
import javax.inject.Inject;

/**
 *
 * @author KDLBL62
 */
public class PlayerService {

    @Inject
    PlayerRepository playerRepository;

    public PlayerService() {
    }
    
    public PlayerService(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    public Player findPlayerById(Long id) {
        return playerRepository.findById(id);
    }

    public void savePlayer(Player player) {
        playerRepository.save(player);
    }

    public void deletePlayer(Long id) {
        playerRepository.delete(id);
    }

    public List<Player> findAllPlayer() {
        return playerRepository.findAll();
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.maven.volleyballKlassement.dto;

import java.util.List;

/**
 *
 * @author KDLBL62
 */
public class ClubDTO {
    
    private Long id;
    private String naam,locatie;
    private List<TeamDTO> teamList;

    public ClubDTO(Long id, String naam, String locatie) {
        this.id = id;
        this.naam = naam;
        this.locatie = locatie;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public String getLocatie() {
        return locatie;
    }

    public void setLocatie(String locatie) {
        this.locatie = locatie;
    }

    public List<TeamDTO> getTeamList() {
        return teamList;
    }

    public void setTeamList(List<TeamDTO> teamList) {
        this.teamList = teamList;
    }
    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.maven.volleyballKlassement.repository;

import com.realdolmen.maven.volleyballKlassement.domain.Club;
import javax.persistence.EntityManager;

/**
 *
 * @author demun
 */
public class ClubRepository extends AbstractRepository<Club, Long> {

    public ClubRepository(EntityManager em) {
        super(em, Club.class);
    }

    public ClubRepository() {
        super(Club.class);
    }
    public Long findByName(String naam){
        return em.createNamedQuery(Club.FIND_LIKE_NAME, Long.class)
                .setParameter("naam", naam).setMaxResults(1).getSingleResult();

    }

}

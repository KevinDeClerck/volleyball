/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.maven.volleyballKlassement.mapper;

import com.realdolmen.maven.volleyballKlassement.domain.Competition;
import com.realdolmen.maven.volleyballKlassement.domain.Game;
import com.realdolmen.maven.volleyballKlassement.domain.Team;
import com.realdolmen.maven.volleyballKlassement.dto.CompetitionDTO;
import com.realdolmen.maven.volleyballKlassement.dto.GameDTO;
import com.realdolmen.maven.volleyballKlassement.dto.TeamDTO;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 *
 * @author KDLBL62
 */
public class CompetitionMapper implements Function<Competition,CompetitionDTO> {

    @Override
    public CompetitionDTO apply(Competition comp) {
        CompetitionDTO dto = new CompetitionDTO(comp.getId(),comp.getNaam(),comp.getDivision(),comp.getRegion());
        if (comp.getTeamList() != null){
        List<Team> teams = comp.getTeamList();
        List<TeamDTO> teamLijst = teams.stream().map(teamL -> new TeamMapper().apply(teamL)).collect(Collectors.toList());
            dto.setTeamList(teamLijst);
        }
        if (comp.getGameList() != null){
            List<Game> games = comp.getGameList();
            List<GameDTO> gameList = games.stream().map(game -> new GameMapper().apply(game)).collect(Collectors.toList());
            dto.setGameList(gameList);
        }
    return dto;
    }
    
}

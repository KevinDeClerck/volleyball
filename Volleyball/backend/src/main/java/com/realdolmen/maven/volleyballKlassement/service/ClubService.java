/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.maven.volleyballKlassement.service;

import com.realdolmen.maven.volleyballKlassement.domain.Club;
import com.realdolmen.maven.volleyballKlassement.repository.ClubRepository;
import java.util.List;
import javax.inject.Inject;

/**
 *
 * @author KDLBL62
 */
public class ClubService {
    
    @Inject
    private ClubRepository clubRepository;

    public ClubService() {
    }

    public ClubService(ClubRepository clubRepository) {
        this.clubRepository = clubRepository;
    }
    
    public Club findClubById (Long id){
        return clubRepository.findById(id);
    }
    
    public Long findClubByNaam(String naam){
        return clubRepository.findByName(naam);
    }
    
    public void saveClub (Club club){
        clubRepository.save(club);
    }
    
    public void deleteClub (Long id){
        clubRepository.delete(id);
    }
    
    public List<Club> findAllClubs (){
        return clubRepository.findAll();
    }
}

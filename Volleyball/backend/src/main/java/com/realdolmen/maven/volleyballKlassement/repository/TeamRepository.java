/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.maven.volleyballKlassement.repository;

import com.realdolmen.maven.volleyballKlassement.domain.Team;
import javax.persistence.EntityManager;

/**
 *
 * @author demun
 */
public class TeamRepository extends AbstractRepository<Team, Long>{
    
    public TeamRepository(EntityManager em) {
        super(em, Team.class);
    }

    public TeamRepository() {
        super(Team.class);
    }
    
    
}
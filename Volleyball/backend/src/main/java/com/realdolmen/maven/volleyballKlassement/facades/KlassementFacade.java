/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.maven.volleyballKlassement.facades;

import static com.google.common.io.Files.map;
import com.realdolmen.maven.volleyballKlassement.domain.Team;
import com.realdolmen.maven.volleyballKlassement.service.CompetitionService;
import com.realdolmen.maven.volleyballKlassement.service.GameService;
import com.realdolmen.maven.volleyballKlassement.service.TeamService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import static java.util.stream.Collectors.toMap;
import javax.inject.Inject;

/**
 *
 * @author Joren
 */
public class KlassementFacade {

    @Inject
    CompetitionService competitionService;

    @Inject
    TeamService teamService;

    @Inject
    GameService gameService;
    
    public int getScore(Long id){
        return gameService.calculatePointsFromTeam(id);
    }
    
    public int getGew3_0(Long id){
        return gameService.getNumber3_0_Or_3_1WonGamesFromTeam(id);
    }
    public int getGew3_2(Long id){
        return gameService.getNumber3_2WonGamesFromTeam(id);
    }
   
    public int getVer3_0(Long id){
        return gameService.getNumber3_0_Or_3_1LostGamesFromTeam(id);
    }
    
    public int getVer3_2(Long id){
        return gameService.getNumber3_2LostGamesFromTeam(id);
    }
    
    public int getGewSets(Long id){
        return gameService.calculateSetsWonByTeam(id);
    }
    
    public int getVerSets(Long id){
        return gameService.calculateSetsLostByTeam(id);
    }
    
    public int getAantalGames(Long id){
        return gameService.getNumberGamesFromTeam(id);
    }
    
    public List<Team> getRank(Long id){
        List<Team> list = new ArrayList<>();
        for(Long id2: getRanking(id).keySet()){
            list.add(teamService.findTeamById(id2));
        }
        return list;
    }
    
    

    public Map<Long, Integer> getRanking(Long id) {
        List<Team> teamsComp = competitionService.findCompetitionById(id).getTeamList();
        Map<Long, Integer> map = new HashMap<>();
        for (Team t : teamsComp) {
            
            map.put(t.getId(), gameService.calculatePointsFromTeam(t.getId()));
        }
        return sortByValue(map);
    }

    public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
        List<Entry<K, V>> list = new ArrayList<>(map.entrySet());
        list.sort(Collections.reverseOrder(Entry.comparingByValue()));

        Map<K, V> result = new LinkedHashMap<>();
        for (Entry<K, V> entry : list) {
            result.put(entry.getKey(), entry.getValue());
            
        }
        return result;
        
    }
}



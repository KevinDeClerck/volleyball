/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.maven.volleyballKlassement.mapper;

import com.realdolmen.maven.volleyballKlassement.domain.Club;
import com.realdolmen.maven.volleyballKlassement.domain.Team;
import com.realdolmen.maven.volleyballKlassement.dto.ClubDTO;
import com.realdolmen.maven.volleyballKlassement.dto.TeamDTO;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 *
 * @author KDLBL62
 */
public class ClubMapper implements Function<Club, ClubDTO> {

    @Override
    public ClubDTO apply(Club club) {
        ClubDTO dto = new ClubDTO(club.getId(),club.getNaam(),club.getLocatie());
        if(club.getTeamList() != null){
            List<Team> teams = club.getTeamList();
            List<TeamDTO> teamLijst = teams.stream().map(teamL -> new TeamMapper().apply(teamL)).collect(Collectors.toList());
            dto.setTeamList(teamLijst);
        }
        return dto;
    }
    
}

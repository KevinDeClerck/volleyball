/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.maven.volleyballKlassement.dto;

import java.util.Date;

/**
 *
 * @author KDLBL62
 */
public class GameDTO {
    
    private Long id;
    private String plaats;
    private String thuisPloeg;
    private String uitPloeg;
    private Date gameDate;
    private Integer resultaatThuisploeg;
    private Integer resultaatUitploeg;
    private String competition;

    public GameDTO(Long id, String plaats, String thuisPloeg, String uitPloeg, Date gameDate, Integer resultaatThuisploeg, Integer resultaatUitploeg, String competition) {
        this.id = id;
        this.plaats = plaats;
        this.thuisPloeg = thuisPloeg;
        this.uitPloeg = uitPloeg;
        this.gameDate = gameDate;
        this.resultaatThuisploeg = resultaatThuisploeg;
        this.resultaatUitploeg = resultaatUitploeg;
        this.competition = competition;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPlaats() {
        return plaats;
    }

    public void setPlaats(String plaats) {
        this.plaats = plaats;
    }

    public String getThuisPloeg() {
        return thuisPloeg;
    }

    public void setThuisPloeg(String thuisPloeg) {
        this.thuisPloeg = thuisPloeg;
    }

    public String getUitPloeg() {
        return uitPloeg;
    }

    public void setUitPloeg(String uitPloeg) {
        this.uitPloeg = uitPloeg;
    }

    public Date getGameDate() {
        return gameDate;
    }

    public void setGameDate(Date gameDate) {
        this.gameDate = gameDate;
    }

    public Integer getResultaatThuisploeg() {
        return resultaatThuisploeg;
    }

    public void setResultaatThuisploeg(Integer resultaatThuisploeg) {
        this.resultaatThuisploeg = resultaatThuisploeg;
    }

    public Integer getResultaatUitploeg() {
        return resultaatUitploeg;
    }

    public void setResultaatUitploeg(Integer resultaatUitploeg) {
        this.resultaatUitploeg = resultaatUitploeg;
    }

    public String getCompetition() {
        return competition;
    }

    public void setCompetition(String competition) {
        this.competition = competition;
    }
    
    
}

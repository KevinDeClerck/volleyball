/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.maven.volleyballKlassement.repository;

import com.realdolmen.maven.volleyballKlassement.domain.Game;
import javax.persistence.EntityManager;

/**
 *
 * @author Joren
 */
public class GameRepository extends AbstractRepository<Game, Long> {

    public GameRepository(EntityManager em) {
        super(em, Game.class);
    }

    public GameRepository() {
        super(Game.class);
    }

}

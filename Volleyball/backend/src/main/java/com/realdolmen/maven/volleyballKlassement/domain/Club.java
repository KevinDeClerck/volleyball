/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.maven.volleyballKlassement.domain;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author demun
 */
@Entity
@Table(name="club")
@NamedQueries({
        @NamedQuery(name = Club.FIND_LIKE_NAME, query = "select id from Club where naam like CONCAT('%',:name,'%')")})
public class Club implements Serializable{
    
    public static final String FIND_LIKE_NAME = "findLikeName";
    @Id
     @GeneratedValue
     private Long id;
    
    private String naam;
    private String locatie;
    
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "club_id")
    private List<Team> teamList;
    
    
    
    public Club(){
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public String getLocatie() {
        return locatie;
    }

    public void setLocatie(String locatie) {
        this.locatie = locatie;
    }

    public List<Team> getTeamList() {
        return teamList;
    }

    public void setTeamList(List<Team> teamList) {
        this.teamList = teamList;
    }

    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.maven.volleyballKlassement.dto;

import com.realdolmen.maven.volleyballKlassement.domain.Club;
import com.realdolmen.maven.volleyballKlassement.domain.Competition;
import java.util.List;

/**
 *
 * @author KDLBL62
 */
public class TeamDTO {
    
    private Long id;
    private String naam;
    private List<PlayerDTO> playerList;
    private Club club;
    private Competition competition;

    public TeamDTO(Long id, String naam, Club club, Competition competition) {
        this.id = id;
        this.naam = naam;
        this.club = club;
        this.competition = competition;
    }

    public Club getClub() {
        return club;
    }

    public void setClub(Club club) {
        this.club = club;
    }

    public Competition getCompetition() {
        return competition;
    }

    public void setCompetition(Competition competition) {
        this.competition = competition;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public List<PlayerDTO> getPlayerList() {
        return playerList;
    }

    public void setPlayerList(List<PlayerDTO> playerList) {
        this.playerList = playerList;
    }
    
    
    
}

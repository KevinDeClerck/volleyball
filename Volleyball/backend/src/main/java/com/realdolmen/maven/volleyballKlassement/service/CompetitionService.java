/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.maven.volleyballKlassement.service;

import com.realdolmen.maven.volleyballKlassement.domain.Competition;
import com.realdolmen.maven.volleyballKlassement.repository.CompetitionRepository;
import java.util.List;
import javax.inject.Inject;

/**
 *
 * @author KDLBL62
 */
public class CompetitionService {

    @Inject
    CompetitionRepository competitionRepository;

    public CompetitionService() {
    }

    public CompetitionService(CompetitionRepository competitionRepository) {
        this.competitionRepository = competitionRepository;
    }

    public Competition findCompetitionById(Long id) {
        return competitionRepository.findById(id);
    }

    public void saveCompetition(Competition competition) {
        competitionRepository.save(competition);
    }

    public void deleteCompetition(Long id) {
        competitionRepository.delete(id);
    }

    public List<Competition> findAllCompetitions() {
        return competitionRepository.findAll();
    }

}

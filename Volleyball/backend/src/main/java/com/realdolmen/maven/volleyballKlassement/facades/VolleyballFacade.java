package com.realdolmen.maven.volleyballKlassement.facades;

import com.realdolmen.maven.volleyballKlassement.domain.Club;
import com.realdolmen.maven.volleyballKlassement.domain.Competition;
import com.realdolmen.maven.volleyballKlassement.domain.Game;
import com.realdolmen.maven.volleyballKlassement.domain.Player;
import com.realdolmen.maven.volleyballKlassement.domain.Team;
import com.realdolmen.maven.volleyballKlassement.dto.ClubDTO;
import com.realdolmen.maven.volleyballKlassement.dto.CompetitionDTO;
import com.realdolmen.maven.volleyballKlassement.dto.GameDTO;
import com.realdolmen.maven.volleyballKlassement.dto.PlayerDTO;
import com.realdolmen.maven.volleyballKlassement.dto.TeamDTO;
import com.realdolmen.maven.volleyballKlassement.mapper.ClubMapper;
import com.realdolmen.maven.volleyballKlassement.mapper.CompetitionMapper;
import com.realdolmen.maven.volleyballKlassement.mapper.GameMapper;
import com.realdolmen.maven.volleyballKlassement.mapper.PlayerMapper;
import com.realdolmen.maven.volleyballKlassement.mapper.TeamMapper;
import com.realdolmen.maven.volleyballKlassement.service.ClubService;
import com.realdolmen.maven.volleyballKlassement.service.CompetitionService;
import com.realdolmen.maven.volleyballKlassement.service.GameService;
import com.realdolmen.maven.volleyballKlassement.service.PlayerService;
import com.realdolmen.maven.volleyballKlassement.service.TeamService;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import javax.inject.Inject;

public class VolleyballFacade implements Serializable{

    @Inject
    private CompetitionService competitionService;
    
    @Inject
    private TeamService teamService;
    
    @Inject
    private ClubService clubService;
    
    @Inject
    private GameService gameService;
    
    @Inject
    private PlayerService playerService;
    
    public Long findClubByName(String naam){
        Long c = clubService.findClubByNaam(naam);
        if (c != null){
            return c;
        }
        return null;
    }

    public List<CompetitionDTO> getAllComps() {
        List<Competition> comps = competitionService.findAllCompetitions();
        return comps.stream()
                .map(comp -> new CompetitionMapper().apply(comp))
                .collect(Collectors.toList());
    }
    
    public List<TeamDTO> getAllTeams(){
        List<Team> teams = teamService.findAllTeams();
        return teams.stream()
                .map(team -> new TeamMapper().apply(team))
                .collect(Collectors.toList());
    }

    public CompetitionDTO getCompById(Long id) {
        Competition c = competitionService.findCompetitionById(id);
        if (c != null){
            return new CompetitionMapper().apply(c);
        }
        return null;
    }
    
    
    public TeamDTO getTeamById(Long id){
        Team t = teamService.findTeamById(id);
        if (t != null){
            return new TeamMapper().apply(t);
        }
        return null;
    }
    
    public List<ClubDTO> getAllClubs() {
        List<Club> clubs = clubService.findAllClubs();
        return clubs.stream()
                .map(club -> new ClubMapper().apply(club))
                .collect(Collectors.toList());
    }
    
    public ClubDTO getClubById(Long id){
        Club c = clubService.findClubById(id);
        if (c != null){
            return new ClubMapper().apply(c);
        }
        return null;
    }
    
    public List<PlayerDTO> getAllPlayers() {
        List<Player> players = playerService.findAllPlayer();
        return players.stream()
                .map(player -> new PlayerMapper().apply(player))
                .collect(Collectors.toList());
    }
    
    public PlayerDTO getPlayerById(Long id){
        Player p = playerService.findPlayerById(id);
        if (p != null){
            return new PlayerMapper().apply(p);
        }
        return null;
    }
    
    public List<GameDTO> getAllGames() {
        List<Game> games = gameService.findAllGames();
        return games.stream()
                .map(game -> new GameMapper().apply(game))
                .collect(Collectors.toList());
    }
    
    public GameDTO getGameById(Long id){
        Game g = gameService.findGameById(id);
        if (g != null){
            return new GameMapper().apply(g);
        }
        return null;
    }
    

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.maven.volleyballKlassement.mapper;

import com.realdolmen.maven.volleyballKlassement.domain.Club;
import com.realdolmen.maven.volleyballKlassement.domain.Player;
import com.realdolmen.maven.volleyballKlassement.domain.Team;
import com.realdolmen.maven.volleyballKlassement.dto.ClubDTO;
import com.realdolmen.maven.volleyballKlassement.dto.PlayerDTO;
import com.realdolmen.maven.volleyballKlassement.dto.TeamDTO;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 *
 * @author KDLBL62
 */
public class TeamMapper implements Function<Team,TeamDTO> {

    @Override
    public TeamDTO apply(Team team) {
        TeamDTO dto = new TeamDTO(team.getId(),team.getNaam(),team.getClub(), team.getCompetition());
        if(team.getPlayerList() !=null){
            List<Player> players = team.getPlayerList();
            List<PlayerDTO> playerList = players.stream().map(player -> new PlayerMapper().apply(player)).collect(Collectors.toList());
            dto.setPlayerList(playerList);
        }    
        return dto;
    }
    
}

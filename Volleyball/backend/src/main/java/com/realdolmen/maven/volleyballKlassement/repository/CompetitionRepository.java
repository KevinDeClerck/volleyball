/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.maven.volleyballKlassement.repository;

import com.realdolmen.maven.volleyballKlassement.domain.Competition;
import javax.persistence.EntityManager;

/**
 *
 * @author demun
 */
public class CompetitionRepository extends AbstractRepository<Competition, Long>{
    
    public CompetitionRepository(EntityManager em) {
        super(em, Competition.class);
    }

    public CompetitionRepository() {
        super(Competition.class);
    }
    
    
    
}

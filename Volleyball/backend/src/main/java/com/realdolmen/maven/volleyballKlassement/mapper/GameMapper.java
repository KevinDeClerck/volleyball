/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.maven.volleyballKlassement.mapper;

import com.realdolmen.maven.volleyballKlassement.domain.Game;
import com.realdolmen.maven.volleyballKlassement.dto.GameDTO;
import java.util.function.Function;

/**
 *
 * @author KDLBL62
 */
public class GameMapper implements Function<Game, GameDTO> {

    @Override
    public GameDTO apply(Game game) {
        GameDTO dto = new GameDTO(game.getId(),game.getPlaats(),game.getThuisPloeg().getNaam(),game.getUitPloeg().getNaam(),game.getGameDate(),game.getResultaatThuisploeg(),game.getResultaatUitploeg(),game.getCompetition().getNaam());
        return dto;
    }
    
}

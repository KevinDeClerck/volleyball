/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.maven.volleyballKlassement.dto;

import java.util.List;

/**
 *
 * @author KDLBL62
 */
public class CompetitionDTO {
    private Long id;
    private String naam,division,region;
    private List<TeamDTO> teamList;
    private List<GameDTO> gameList;
    private Boolean active;

    public CompetitionDTO(Long id, String naam, String division, String region) {
        this.id = id;
        this.naam = naam;
        this.division = division;
        this.region = region;
    }

    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public List<TeamDTO> getTeamList() {
        return teamList;
    }

    public void setTeamList(List<TeamDTO> teamList) {
        this.teamList = teamList;
    }

    public List<GameDTO> getGameList() {
        return gameList;
    }

    public void setGameList(List<GameDTO> gameList) {
        this.gameList = gameList;
    }
    
    
}

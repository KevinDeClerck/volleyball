/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.maven.volleyballKlassement.domain;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author demun
 */
@Entity
public class Game {
    @Id
    @GeneratedValue
    private Long id;
    
    private String plaats;
    
    @ManyToOne
    @JoinColumn(name = "thuisploeg")
    private Team thuisPloeg;
    @ManyToOne
    @JoinColumn(name = "uitploeg")
    private Team uitPloeg;
    
    
    @Temporal(TemporalType.DATE)
    @Column(nullable = true)
    private Date gameDate;
    
    private Integer resultaatThuisploeg;
    private Integer resultaatUitploeg;
    
    @ManyToOne
    private Competition competition;
    
    public Game(){
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Team getThuisPloeg() {
        return thuisPloeg;
    }

    public void setThuisPloeg(Team thuisPloeg) {
        this.thuisPloeg = thuisPloeg;
    }

    public Team getUitPloeg() {
        return uitPloeg;
    }

    public void setUitPloeg(Team uitPloeg) {
        this.uitPloeg = uitPloeg;
    }

    public Date getGameDate() {
        return gameDate;
    }

    public void setGameDate(Date gameDate) {
        this.gameDate = gameDate;
    }

    public Integer getResultaatThuisploeg() {
        return resultaatThuisploeg;
    }

    public void setResultaatThuisploeg(Integer resultaatThuisploeg) {
        this.resultaatThuisploeg = resultaatThuisploeg;
    }

    public Integer getResultaatUitploeg() {
        return resultaatUitploeg;
    }

    public void setResultaatUitploeg(Integer resultaatUitploeg) {
        this.resultaatUitploeg = resultaatUitploeg;
    }

    public Competition getCompetition() {
        return competition;
    }

    public void setCompetition(Competition competition) {
        this.competition = competition;
    }

    public String getPlaats() {
        return plaats;
    }

    public void setPlaats(String plaats) {
        this.plaats = plaats;
    }
    
}


insert into competition(id, active, division, naam, region) values(1000, true, 'Eerste provinciale', 'bv-1ste provinciale A', 'Vlaams-brabant');
insert into competition(id, active, division, naam, region) values(2000, true, 'Tweede provinciale', 'bv-2de provinciale A', 'Vlaams-brabant');

insert into club(id, locatie, naam) values(1000, 'Lotstraat 39', 'Davoc lot');
insert into club(id, locatie, naam) values(2000, 'Keerbergenstraat 20', 'FC Keerbergen');
insert into club(id, locatie, naam) values(3000, 'Zuunstraat 25', 'FC Zuun');
insert into club(id, locatie, naam) values(4000, 'Averbodestraat 120', 'VC Averbode');
insert into club(id, locatie, naam) values(5000, 'Leefdaalsesteenweg 35', 'FC Leefdaal');
insert into club(id, locatie, naam) values(6000, 'Haasrodeberg 10', 'VC Haasrode');
insert into club(id, locatie, naam) values(7000, 'Hofstaadsedreef 19', 'VC Hofstaade');
insert into club(id, locatie, naam) values(8000, 'Wijgmaalse drempel 88', 'FC Wijgmaal');
insert into club(id, locatie, naam) values(9000, 'Ninoofsesteenweg 69', 'VC Schepdaal');
insert into club(id, locatie, naam) values(10000, 'Haacht op den Berg 255', 'FC Haacht');
insert into club(id, locatie, naam) values(11000, 'Oude markt 192', 'VC Leuven');
insert into club(id, locatie, naam) values(12000, 'Kerkstraat 404', 'FC Beersel');
insert into club(id, locatie, naam) values(13000, 'Beerselbaan 999', 'VC Drogenbos');
insert into club(id, locatie, naam) values(14000, 'Stationstraat 18', 'VCA Huizingen');
insert into club(id, locatie, naam) values(15000, 'Edingsesteenweg 16', 'VCV Gooik');
insert into club(id, locatie, naam) values(16000, 'Pososplein 77', 'VCV Halle');

insert into team(id, naam, club_id, competition_id) values(1000, 'Lot A', 1000, 1000);
insert into team(id, naam, club_id, competition_id) values(2000, 'Keerbergen A', 2000, 1000);
insert into team(id, naam, club_id, competition_id) values(3000, 'Zuun A', 3000, 1000);
insert into team(id, naam, club_id, competition_id) values(4000, 'Averbode A', 4000, 1000);
insert into team(id, naam, club_id, competition_id) values(5000, 'Leefdaal A', 5000, 1000);
insert into team(id, naam, club_id, competition_id) values(6000, 'Haasrode A', 6000, 1000);
insert into team(id, naam, club_id, competition_id) values(7000, 'Hofstaade A', 7000, 1000);
insert into team(id, naam, club_id, competition_id) values(8000, 'Wijgmaal A', 8000, 1000);
insert into team(id, naam, club_id, competition_id) values(9000, 'Schepdaal A', 9000, 1000);
insert into team(id, naam, club_id, competition_id) values(10000, 'Haacht A', 10000, 1000);
insert into team(id, naam, club_id, competition_id) values(11000, 'Leuven A', 11000, 1000);
insert into team(id, naam, club_id, competition_id) values(12000, 'Beersel A', 12000, 1000);
insert into team(id, naam, club_id, competition_id) values(13000, 'Drogenbos A', 13000, 1000);
insert into team(id, naam, club_id, competition_id) values(14000, 'Huizingen A', 14000, 1000);
insert into team(id, naam, club_id, competition_id) values(15000, 'Gooik A', 15000, 1000);
insert into team(id, naam, club_id, competition_id) values(16000, 'Halle A', 16000, 1000);

insert into team(id, naam, club_id, competition_id) values(17000, 'Lot B', 1000, 2000);
insert into team(id, naam, club_id, competition_id) values(18000, 'Keerbergen B', 2000, 2000);
insert into team(id, naam, club_id, competition_id) values(19000, 'Zuun B', 3000, 2000);
insert into team(id, naam, club_id, competition_id) values(20000, 'Averbode B', 4000, 2000);
insert into team(id, naam, club_id, competition_id) values(21000, 'Leefdaal B', 5000, 2000);
insert into team(id, naam, club_id, competition_id) values(22000, 'Haasrode B', 6000, 2000);
insert into team(id, naam, club_id, competition_id) values(23000, 'Hofstaade B', 7000, 2000);
insert into team(id, naam, club_id, competition_id) values(24000, 'Wijgmaal B', 8000, 2000);
insert into team(id, naam, club_id, competition_id) values(25000, 'Schepdaal B', 9000, 2000);
insert into team(id, naam, club_id, competition_id) values(26000, 'Haacht B', 10000, 2000);
insert into team(id, naam, club_id, competition_id) values(27000, 'Leuven B', 11000, 2000);
insert into team(id, naam, club_id, competition_id) values(28000, 'Beersel B', 12000, 2000);
insert into team(id, naam, club_id, competition_id) values(29000, 'Drogenbos B', 13000, 2000);
insert into team(id, naam, club_id, competition_id) values(30000, 'Huizingen B', 14000, 2000);
insert into team(id, naam, club_id, competition_id) values(31000, 'Gooik B', 15000, 2000);
insert into team(id, naam, club_id, competition_id) values(32000, 'Halle B', 16000, 2000);


insert into game(id, gameDate, plaats, resultaatThuisploeg, resultaatUitploeg, competition_id, thuisploeg, uitploeg) values (1000, '2018-08-06', 'Lot', 3, 0, 1000, 1000, 2000);
insert into game(id, gameDate, plaats, resultaatThuisploeg, resultaatUitploeg, competition_id, thuisploeg, uitploeg) values (2000, '2018-08-06', 'Zuun', 1, 3, 1000, 3000, 4000);
insert into game(id, gameDate, plaats, resultaatThuisploeg, resultaatUitploeg, competition_id, thuisploeg, uitploeg) values (3000, '2018-08-06', 'Leefdaal', 2, 3, 1000, 5000, 6000);
insert into game(id, gameDate, plaats, resultaatThuisploeg, resultaatUitploeg, competition_id, thuisploeg, uitploeg) values (4000, '2018-08-06', 'Hofstade', 3, 1, 1000, 7000, 8000);
insert into game(id, gameDate, plaats, resultaatThuisploeg, resultaatUitploeg, competition_id, thuisploeg, uitploeg) values (5000, '2018-08-06', 'Schepdaal', 2, 3, 1000, 9000, 10000);
insert into game(id, gameDate, plaats, resultaatThuisploeg, resultaatUitploeg, competition_id, thuisploeg, uitploeg) values (6000, '2018-08-06', 'Leuven', 1, 3, 1000, 11000, 12000);
insert into game(id, gameDate, plaats, resultaatThuisploeg, resultaatUitploeg, competition_id, thuisploeg, uitploeg) values (7000, '2018-08-06', 'Drogenbos', 3, 2, 1000, 13000, 14000);
insert into game(id, gameDate, plaats, resultaatThuisploeg, resultaatUitploeg, competition_id, thuisploeg, uitploeg) values (8000, '2018-08-06', 'Gooik', 3, 0, 1000, 15000, 16000);
insert into game(id, gameDate, plaats, resultaatThuisploeg, resultaatUitploeg, competition_id, thuisploeg, uitploeg) values (9000, '2018-08-13', 'Lot', 3, 1, 1000, 1000, 16000);
insert into game(id, gameDate, plaats, resultaatThuisploeg, resultaatUitploeg, competition_id, thuisploeg, uitploeg) values (10000, '2018-08-13', 'Keerbergen', 2, 3, 1000, 2000, 3000);
insert into game(id, gameDate, plaats, resultaatThuisploeg, resultaatUitploeg, competition_id, thuisploeg, uitploeg) values (11000, '2018-08-13', 'Averbode', 3, 1, 1000, 4000, 5000);
insert into game(id, gameDate, plaats, resultaatThuisploeg, resultaatUitploeg, competition_id, thuisploeg, uitploeg) values (12000, '2018-08-13', 'Haasrode', 3, 0, 1000, 6000, 7000);
insert into game(id, gameDate, plaats, resultaatThuisploeg, resultaatUitploeg, competition_id, thuisploeg, uitploeg) values (13000, '2018-08-13', 'Wijgmaal', 3, 1, 1000, 8000, 9000);
insert into game(id, gameDate, plaats, resultaatThuisploeg, resultaatUitploeg, competition_id, thuisploeg, uitploeg) values (14000, '2018-08-13', 'Haacht', 1, 3, 1000, 10000, 11000);
insert into game(id, gameDate, plaats, resultaatThuisploeg, resultaatUitploeg, competition_id, thuisploeg, uitploeg) values (15000, '2018-08-13', 'Beersel', 3, 1, 1000, 12000, 13000);
insert into game(id, gameDate, plaats, resultaatThuisploeg, resultaatUitploeg, competition_id, thuisploeg, uitploeg) values (16000, '2018-08-13', 'Huizingen', 3, 0, 1000, 14000, 15000);
insert into game(id, gameDate, plaats, resultaatThuisploeg, resultaatUitploeg, competition_id, thuisploeg, uitploeg) values (17000, '2018-08-20', 'Keerbergen', 3, 0, 1000, 2000, 16000);
insert into game(id, gameDate, plaats, resultaatThuisploeg, resultaatUitploeg, competition_id, thuisploeg, uitploeg) values (18000, '2018-08-20', 'Lot', 3, 2, 1000, 1000, 3000);
insert into game(id, gameDate, plaats, resultaatThuisploeg, resultaatUitploeg, competition_id, thuisploeg, uitploeg) values (19000, '2018-08-20', 'Averbode', 2, 3, 1000, 4000, 6000);
insert into game(id, gameDate, plaats, resultaatThuisploeg, resultaatUitploeg, competition_id, thuisploeg, uitploeg) values (20000, '2018-08-20', 'Leefdaal', 3, 1, 1000, 5000, 7000);
insert into game(id, gameDate, plaats, resultaatThuisploeg, resultaatUitploeg, competition_id, thuisploeg, uitploeg) values (21000, '2018-08-20', 'Wijgmaal', 3, 2, 1000, 8000, 9000);
insert into game(id, gameDate, plaats, resultaatThuisploeg, resultaatUitploeg, competition_id, thuisploeg, uitploeg) values (22000, '2018-08-20', 'Haacht', 3, 0, 1000, 10000, 12000);
insert into game(id, gameDate, plaats, resultaatThuisploeg, resultaatUitploeg, competition_id, thuisploeg, uitploeg) values (23000, '2018-08-20', 'Leuven', 0, 3, 1000, 11000, 13000);
insert into game(id, gameDate, plaats, resultaatThuisploeg, resultaatUitploeg, competition_id, thuisploeg, uitploeg) values (24000, '2018-08-20', 'Halle', 1, 3, 1000, 14000, 15000);



insert into player(id, birthDate, firstName, lastName, team_id) values(1000, '1996-08-06', 'Jelter', 'Demunter', 1000);
insert into player(id, birthDate, firstName, lastName, team_id) values(2000, '1990-05-02', 'Mats', 'Buelinx', 1000);
insert into player(id, birthDate, firstName, lastName, team_id) values(3000, '1987-10-10', 'Jan', 'Jansens', 2000);
insert into player(id, birthDate, firstName, lastName, team_id) values(4000, '1992-09-07', 'Pieter', 'Pieters', 2000);
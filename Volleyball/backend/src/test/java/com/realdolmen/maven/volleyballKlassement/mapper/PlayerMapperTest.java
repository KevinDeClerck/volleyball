/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.maven.volleyballKlassement.mapper;

import com.realdolmen.maven.volleyballKlassement.domain.Player;
import com.realdolmen.maven.volleyballKlassement.dto.PlayerDTO;
import com.realdolmen.maven.volleyballKlassement.mapper.PlayerMapper;
import java.util.Date;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author KDLBL62
 */


public class PlayerMapperTest {
    
    
    
    @Test
    public void applyTest(){
        Player p = new Player();
        p.setId(5000L);
        p.setFirstName("Kevin");
        p.setLastName("De Clerck");
        
        PlayerDTO pd = new PlayerMapper().apply(p);
        Assert.assertEquals(p.getId(),pd.getId());        
        Assert.assertEquals(p.getFirstName(),pd.getFirstName());        
        Assert.assertEquals(p.getLastName(),pd.getLastName());
    }
    
    
}

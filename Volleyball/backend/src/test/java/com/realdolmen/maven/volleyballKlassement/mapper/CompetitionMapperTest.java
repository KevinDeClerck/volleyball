/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.maven.volleyballKlassement.mapper;

import com.realdolmen.maven.volleyballKlassement.domain.Competition;
import com.realdolmen.maven.volleyballKlassement.dto.CompetitionDTO;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author KDLBL62
 */
public class CompetitionMapperTest {
    
    @Test
    public void applyTest(){
        Competition c = new Competition();
        c.setId(5000L);
        c.setDivision("Eerste");
        c.setRegion("Vlaams brabant");
        
        CompetitionDTO cd = new CompetitionMapper().apply(c);
        Assert.assertEquals(c.getId(),cd.getId());        
        Assert.assertEquals(c.getDivision(),cd.getDivision());        
        Assert.assertEquals(c.getRegion(),cd.getRegion());      
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.maven.volleyballKlassement.repository;


import com.realdolmen.maven.volleyballKlassement.domain.Team;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author demun
 */
public class TeamRepositoryTest {
    private static EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;
    private static TeamRepository teamRepository;
    
    @BeforeClass
    public static void initClass() {
        entityManagerFactory = Persistence.createEntityManagerFactory("VolleyballPersistenceUnitTest");
    }

    @Before
    public void init() {
        entityManager = entityManagerFactory.createEntityManager();
        teamRepository = new TeamRepository(entityManager);
    }
    
    @Test
    public void findTeamByIdTest(){
        Team team = teamRepository.findById(2L);
        assertNotNull(team);
    }
    
    @Test
    public void saveTeamTest(){
        Team team = new Team();
        team.setNaam("lot");
        teamRepository.save(team);
        
        Team t1 = entityManager.find(Team.class, team.getId());
        assertEquals("lot", t1.getNaam());
    }
    
    @Test
    public void updateTeamTest(){
        Team t = entityManager.find(Team.class,5000L);
        teamRepository.begin();
        entityManager.detach(t);
        t.setNaam("lot");
        entityManager.flush();
        entityManager.clear();
        teamRepository.update(t);
        
        assertEquals("lot", entityManager.find(Team.class, 5000L).getNaam());
    }
    
    @Test
    public void findAllPlayerTest(){
        List<Team> players = teamRepository.findAll();
        assertFalse(players.isEmpty());
    }
    
    @Test
    public void deleteClubTest(){
         teamRepository.delete(3000L);
         assertEquals(null, entityManager.find(Team.class,3000L));
    }
    
     @After
    public void exit() {
        teamRepository.close();
    }

    @AfterClass
    public static void exitClass(){
        entityManagerFactory.close();
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.maven.volleyballKlassement.repository;

import com.realdolmen.maven.volleyballKlassement.domain.Club;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author demun
 */
public class ClubRepositoryTest{
    private static EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;
    private static ClubRepository clubRepository;
    
    @BeforeClass
    public static void initClass() {
        entityManagerFactory = Persistence.createEntityManagerFactory("VolleyballPersistenceUnitTest");
    }

    @Before
    public void init() {
        entityManager = entityManagerFactory.createEntityManager();
        clubRepository = new ClubRepository(entityManager);
    }
    
    @Test
    public void findClubByIdTest(){
        Club club = clubRepository.findById(1000L);
        assertNotNull(club);
    }
    
    @Test
    public void saveClubTest(){
        Club club = new Club();
        club.setLocatie("lot");
        club.setNaam("davoc lot");
        clubRepository.save(club);      
        Club c1 = entityManager.find(Club.class, club.getId());
        assertEquals("lot", c1.getLocatie());
    }
    
    
    
    @Test
    public void updateClubTest(){
        Club c = entityManager.find(Club.class,2000L);
        clubRepository.begin();
        entityManager.detach(c);
        c.setLocatie("keerbergenstraat 20");
        entityManager.flush();
        entityManager.clear();
        clubRepository.update(c);        
        assertEquals("keerbergenstraat 20", entityManager.find(Club.class, 2000L).getLocatie());
    }
    
    @Test
    public void findAllClubTest(){
        List<Club> competitions = clubRepository.findAll();
        assertFalse(competitions.isEmpty());
    }
    
     @Test
    public void deleteClubTest(){
         clubRepository.delete(3000L);
         assertEquals(null, entityManager.find(Club.class,3000L));
    }
 
    @After
    public void exit() {
        clubRepository.close();
    }

    @AfterClass
    public static void exitClass(){
        entityManagerFactory.close();
    }
    
}

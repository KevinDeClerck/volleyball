/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.maven.volleyballKlassement.service;


import com.realdolmen.maven.volleyballKlassement.domain.Player;
import com.realdolmen.maven.volleyballKlassement.repository.PlayerRepository;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author KDLBL62
 */

@RunWith(MockitoJUnitRunner.class)
public class PlayerServiceTest {
   
    @Mock
    private PlayerRepository playerRepository;
    
    @InjectMocks
    private PlayerService playerService;
    
    private List<Player> players;
    
    @Before
    public void init(){
        players = new ArrayList<>();
        
    }
    
    @Test
    public void findAllPlayersTest(){
        when(playerRepository.findAll()).thenReturn(new ArrayList<>(Arrays.asList(new Player())));
        
        List<Player>result =playerService.findAllPlayer();
        
        assertEquals(result.size(),1);
        Mockito.verify(playerRepository,Mockito.times(1)).findAll();
    }
  
    @Test
    public void findPlayerByIdTest(){
        Player player = new Player();
        when(playerRepository.findById(1000L)).thenReturn(player);
        Player result = playerService.findPlayerById(1000L);
        assertEquals(player, result);
        verify(playerRepository, times(1)).findById(1000L);
    }
    
    
}

package com.realdolmen.maven.volleyballKlassement.repository;

import com.realdolmen.maven.volleyballKlassement.domain.Competition;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import org.junit.Before;
import org.junit.BeforeClass;

public class CompetitionRepositoryTest {
    private static EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;
    private static CompetitionRepository competitionRepository;
    
    
    @BeforeClass
    public static void initClass() {
        entityManagerFactory = Persistence.createEntityManagerFactory("VolleyballPersistenceUnitTest");
    }

    @Before
    public void init() {
        entityManager = entityManagerFactory.createEntityManager();
        competitionRepository = new CompetitionRepository(entityManager);
    }


    @Test
    public void findCompetitionById(){
        Competition competition = competitionRepository.findById(1000L);
        assertNotNull(competition);
    }
    
    @Test
    public void saveCompetitionTest(){
        Competition competition = new Competition();
        competition.setDivision("1ste klass");
        competition.setRegion("vlaams-brabant");
        competitionRepository.save(competition);
        
        Competition c1 = entityManager.find(Competition.class, competition.getId());
        assertEquals("1ste klass", c1.getDivision());
    }
    

    
    @Test
    public void updateCompetitionTest(){
        Competition p = entityManager.find(Competition.class,1000L);
        competitionRepository.begin();
        entityManager.detach(p);
        p.setDivision("1ste klass");
        entityManager.flush();
        entityManager.clear();
        competitionRepository.update(p);
        
        assertEquals("1ste klass", entityManager.find(Competition.class, 1000L).getDivision());
    }
    
    @Test
    public void findAllCompetitionTest(){
        List<Competition> competitions = competitionRepository.findAll();
        assertFalse(competitions.isEmpty());
    }
    
    
    @After
    public void exit() {
        competitionRepository.close();
    }

    @AfterClass
    public static void exitClass(){
        entityManagerFactory.close();
    }
}

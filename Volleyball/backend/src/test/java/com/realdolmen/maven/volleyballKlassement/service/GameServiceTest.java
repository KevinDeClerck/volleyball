/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.maven.volleyballKlassement.service;

import com.realdolmen.maven.volleyballKlassement.domain.Game;
import com.realdolmen.maven.volleyballKlassement.repository.GameRepository;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author KDLBL62
 */

@RunWith(MockitoJUnitRunner.class)
public class GameServiceTest {
    @Mock
    private GameRepository gameRepository;
    
    @InjectMocks
    private GameService gameService;
    
    private List<Game> games;
    
    @Before
    public void init(){
        games = new ArrayList<>();
        
    }
    
    @Test
    public void findAllGamesTest(){
        when(gameRepository.findAll()).thenReturn(new ArrayList<>(Arrays.asList(new Game())));
        
        List<Game>result =gameService.findAllGames();
        
        assertEquals(result.size(),1);
        Mockito.verify(gameRepository,Mockito.times(1)).findAll();
    }
  
    @Test
    public void findGameByIdTest(){
        Game game = new Game();
        when(gameRepository.findById(1000L)).thenReturn(game);
        Game result = gameService.findGameById(1000L);
        assertEquals(game, result);
        verify(gameRepository, times(1)).findById(1000L);
    }
    
    
}

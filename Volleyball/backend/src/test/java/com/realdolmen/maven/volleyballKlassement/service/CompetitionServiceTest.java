/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.maven.volleyballKlassement.service;

import com.realdolmen.maven.volleyballKlassement.domain.Competition;
import com.realdolmen.maven.volleyballKlassement.repository.CompetitionRepository;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author KDLBL62
 */
@RunWith(MockitoJUnitRunner.class)
public class CompetitionServiceTest {
    
    @Mock
    private CompetitionRepository competitionRepository;
    
    @InjectMocks
    private CompetitionService competitionService;
    
    private List<Competition> competitions;
    
    @Before
    public void init(){
        competitions = new ArrayList<>();
    }
    
    @Test
    public void findAllCompetitionsTest(){
        when(competitionRepository.findAll()).thenReturn(new ArrayList<>(Arrays.asList(new Competition())));
        
        List<Competition>result =competitionService.findAllCompetitions();
        
        assertEquals(result.size(),1);
        Mockito.verify(competitionRepository,Mockito.times(1)).findAll();
    }
  
    @Test
    public void findCompetitionByIdTest(){
        Competition competition = new Competition();
        when(competitionRepository.findById(1000L)).thenReturn(competition);
        Competition result = competitionService.findCompetitionById(1000L);
        assertEquals(competition, result);
        verify(competitionRepository, times(1)).findById(1000L);
    }   
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.maven.volleyballKlassement.mapper;

import com.realdolmen.maven.volleyballKlassement.domain.Club;
import com.realdolmen.maven.volleyballKlassement.dto.ClubDTO;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author KDLBL62
 */
public class ClubMapperTest {
    
     @Test
    public void applyTest(){
        Club c = new Club();
        c.setId(5000L);
        c.setLocatie("Lot");
        c.setNaam("Davoc Lot");       
        
        ClubDTO cd = new ClubMapper().apply(c);
        Assert.assertEquals(c.getId(),cd.getId());        
        Assert.assertEquals(c.getLocatie(),cd.getLocatie());       
        Assert.assertEquals(c.getNaam(),cd.getNaam());
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.maven.volleyballKlassement.repository;


import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
public class AbstractPersistenceTest {
    private static EntityManagerFactory emf;

    protected EntityManager em;
    private EntityTransaction etx;

    @BeforeClass
    public static void initializeEntityManagerFactory() {      
        emf = Persistence.createEntityManagerFactory("VolleyballPersistenceUnitTest");
    }

    @Before
    public void initializeEntityManagerWithTransaction() {
        System.out.println("Hello before");
        em = emf.createEntityManager();
        etx = em.getTransaction();
        etx.begin();
    }

    @After
    public void rollbackTransactionAndCloseEntityManager() {
        System.out.println("hello after");
        etx.rollback();
        em.close();
    }

    @AfterClass
    public static void destroyEntityManagerFactory() {
        emf.close();
    }
}
 


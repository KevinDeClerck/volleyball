/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.maven.volleyballKlassement.service;

import com.realdolmen.maven.volleyballKlassement.domain.Team;
import com.realdolmen.maven.volleyballKlassement.repository.TeamRepository;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author KDLBL62
 */

@RunWith(MockitoJUnitRunner.class)
public class TeamServiceTest {
    
     @Mock
    private TeamRepository teamRepository;
    
    @InjectMocks
    private TeamService teamService;
    
    private List<Team> teams;
    
    @Before
    public void init(){
        teams = new ArrayList<>();
        
    }
    
    @Test
    public void findAllClubsTest(){
        when(teamRepository.findAll()).thenReturn(new ArrayList<>(Arrays.asList(new Team())));
        
        List<Team>result =teamService.findAllTeams();
        
        assertEquals(result.size(),1);
        Mockito.verify(teamRepository,Mockito.times(1)).findAll();
    }
  
    @Test
    public void findClubByIdTest(){
        Team team = new Team();
        when(teamRepository.findById(1000L)).thenReturn(team);
        Team result = teamService.findTeamById(1000L);
        assertEquals(team, result);
        verify(teamRepository, times(1)).findById(1000L);
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.maven.volleyballKlassement.mapper;

import com.realdolmen.maven.volleyballKlassement.domain.Club;
import com.realdolmen.maven.volleyballKlassement.domain.Competition;
import com.realdolmen.maven.volleyballKlassement.domain.Team;
import com.realdolmen.maven.volleyballKlassement.dto.TeamDTO;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author KDLBL62
 */
public class TeamMapperTest {
    
     @Test
    public void applyTest(){
        Club c = new Club();      
        c.setNaam("Lot");   
        Competition comp = new Competition();       
        comp.setNaam("Eerste");
        
        Team t = new Team();
        t.setId(5000L);
        t.setClub(c);
        t.setCompetition(comp);
        
        TeamDTO td = new TeamMapper().apply(t);
        
        Assert.assertEquals(t.getId(),td.getId());        
        Assert.assertEquals(t.getClub().getNaam(),td.getClub().getNaam());        
        Assert.assertEquals(t.getCompetition().getNaam(),td.getCompetition().getNaam());
    }
    
}

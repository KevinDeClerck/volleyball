/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.maven.volleyballKlassement.repository;

import com.realdolmen.maven.volleyballKlassement.domain.Game;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author KDLBL62
 */
public class GameRepositoryTest{
    private static EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;
    private static GameRepository gameRepository;
    
    
    @BeforeClass
    public static void initClass() {
        entityManagerFactory = Persistence.createEntityManagerFactory("VolleyballPersistenceUnitTest");
    }

    @Before
    public void init() {
        entityManager = entityManagerFactory.createEntityManager();
        gameRepository = new GameRepository(entityManager);
    }

    
    @Test
    public void findGameById(){
        Game game = gameRepository.findById(1000L);
        assertNotNull(game);
    }
    
    @Test
    public void saveGame(){
        Game game = new Game();       
        game.setPlaats("Lot");
        game.setResultaatThuisploeg(3);
        game.setResultaatUitploeg(1);       
        gameRepository.save(game);       
        Game tGame = entityManager.find(Game.class, game.getId());
        assertEquals("Lot", tGame.getPlaats());
    }
    
    
     @Test
    public void updateGameTest(){
        Game g = entityManager.find(Game.class,1000L);
        gameRepository.begin();
        entityManager.detach(g);
        g.setPlaats("Huizingen");
        entityManager.flush();
        entityManager.clear();
        gameRepository.update(g);
        assertEquals("Huizingen", entityManager.find(Game.class, 1000L).getPlaats());
    }

    @Test
    public void findAllGameTest(){
        List<Game> candies = gameRepository.findAll();
        assertFalse(candies.isEmpty());
    }
    
     @Test
    public void deleteGameTest(){
        gameRepository.delete(1000L);
        assertEquals(null, entityManager.find(Game.class,1000L));
    }
    
    
    @After
    public void exit() {
        gameRepository.close();
    }

    @AfterClass
    public static void exitClass(){
        entityManagerFactory.close();
    }
}

    
    


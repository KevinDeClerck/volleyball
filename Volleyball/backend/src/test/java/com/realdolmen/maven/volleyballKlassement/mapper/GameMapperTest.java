/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.maven.volleyballKlassement.mapper;

import com.realdolmen.maven.volleyballKlassement.domain.Competition;
import com.realdolmen.maven.volleyballKlassement.domain.Game;
import com.realdolmen.maven.volleyballKlassement.domain.Team;
import com.realdolmen.maven.volleyballKlassement.dto.GameDTO;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author KDLBL62
 */
public class GameMapperTest {
    
     @Test
    public void applyTest(){
        Competition comp = new Competition();       
        comp.setNaam("Eerste");
        
        Team t = new Team();
        t.setNaam("Lot");
        
        Team t2 = new Team();
        t2.setNaam("Zuun");
        
        Game g = new Game();
        g.setId(5000L);
        g.setCompetition(comp);
        g.setThuisPloeg(t);
        g.setUitPloeg(t2);
        
        GameDTO gd = new GameMapper().apply(g);
        Assert.assertEquals(g.getId(),gd.getId());        
        Assert.assertEquals(g.getCompetition().getNaam(),gd.getCompetition());        
        Assert.assertEquals(g.getThuisPloeg().getNaam(),gd.getThuisPloeg());
        Assert.assertEquals(g.getUitPloeg().getNaam(),gd.getUitPloeg());
    }
    
}

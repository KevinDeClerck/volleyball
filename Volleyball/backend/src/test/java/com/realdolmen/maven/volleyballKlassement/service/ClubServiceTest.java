
package com.realdolmen.maven.volleyballKlassement.service;

import com.realdolmen.maven.volleyballKlassement.domain.Club;
import com.realdolmen.maven.volleyballKlassement.repository.ClubRepository;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ClubServiceTest {
    
    @Mock
    private ClubRepository clubRepository;
    
    @InjectMocks
    private ClubService clubService;
    
    private List<Club> clubs;
    
    @Before
    public void init(){
        clubs = new ArrayList<>();
        
    }
    
    @Test
    public void findAllClubsTest(){
        when(clubRepository.findAll()).thenReturn(new ArrayList<>(Arrays.asList(new Club())));
        
        List<Club>result =clubService.findAllClubs();
        
        assertEquals(result.size(),1);
        Mockito.verify(clubRepository,Mockito.times(1)).findAll();
    }
  
    @Test
    public void findClubByIdTest(){
        Club club = new Club();
        when(clubRepository.findById(1000L)).thenReturn(club);
        Club result = clubService.findClubById(1000L);
        assertEquals(club, result);
        verify(clubRepository, times(1)).findById(1000L);
    }
}

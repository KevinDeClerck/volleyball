/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import com.realdolmen.maven.volleyballKlassement.dto.ClubDTO;
import com.realdolmen.maven.volleyballKlassement.facades.VolleyballFacade;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Joren
 */
@Named
@ApplicationScoped
public class SearchController implements Serializable {
    
    @Inject
    VolleyballFacade volleyballFacade;
    
    ClubDTO club;
 
    @PostConstruct
    public void init(){
        
    }

    public Long getClub(String naam) {
        return volleyballFacade.findClubByName(naam);
    }

    public void setClub(ClubDTO club) {
        this.club = club;
    }
    
    
}

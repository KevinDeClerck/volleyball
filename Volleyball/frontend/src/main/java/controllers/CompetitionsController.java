package controllers;

import com.realdolmen.maven.volleyballKlassement.dto.CompetitionDTO;
import com.realdolmen.maven.volleyballKlassement.facades.VolleyballFacade;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@RequestScoped
public class CompetitionsController implements Serializable {

    @Inject
    private VolleyballFacade volleyballFacade;

    private List<CompetitionDTO> comps;
    //private Competition competition;

    @PostConstruct
    public void init() {
        comps = volleyballFacade.getAllComps();
    }

    public List<CompetitionDTO> getComps() {
        return comps;
    }

    public void setComps(List<CompetitionDTO> comps) {
        this.comps = comps;
    }

    /*public Competition getCompetitionWithId(Long id) {
        return volleyballFacade.getCompById(id);
    }

    public void setCompetition(Competition competition) {
        this.competition = competition;
    }*/
    
    
}

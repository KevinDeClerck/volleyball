package controllers;

import com.realdolmen.maven.volleyballKlassement.dto.ClubDTO;
import com.realdolmen.maven.volleyballKlassement.facades.VolleyballFacade;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author demun
 */
@Named
@RequestScoped
public class ClubController implements Serializable {

    @Inject
    VolleyballFacade volleyballFacade;
    
    private ClubDTO club;
    
    @PostConstruct
    public void init() {
    }

    public ClubDTO getClub() {
        return club;
    }

    public void setClub(Long id) {
       club = volleyballFacade.getClubById(id);
    }
}

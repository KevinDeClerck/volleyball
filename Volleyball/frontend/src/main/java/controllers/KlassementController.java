/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import com.realdolmen.maven.volleyballKlassement.domain.Team;
import com.realdolmen.maven.volleyballKlassement.dto.CompetitionDTO;
import com.realdolmen.maven.volleyballKlassement.facades.KlassementFacade;
import com.realdolmen.maven.volleyballKlassement.facades.VolleyballFacade;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Joren
 */
@Named
@RequestScoped
public class KlassementController implements Serializable{
    @Inject
    private KlassementFacade klassementFacade;

    private List<Team> teamlist;
    private int aantalGespeeld;
    private int punten;
    private int gewon3_0;
    private int gewon3_2;
    private int ver3_0;
    private int ver3_2;
    private int gewSet;
    private int verSet;
    

    @PostConstruct
    public void init() {
    }

    public List<Team> getTeamlist() {
        return teamlist;
    }

    public void setTeamlist(Long id) {
        this.teamlist = klassementFacade.getRank(id);
    }

    public int getAantalGespeeld(Long id) {
        return klassementFacade.getAantalGames(id);
    }

    public void setAantalGespeeld(int aantalGespeeld) {
        this.aantalGespeeld = aantalGespeeld;
    }

    public int getGewon3_0(Long id) {
        return klassementFacade.getGew3_0(id);
    }

    public void setGewon3_0(int gewon3_0) {
        this.gewon3_0 = gewon3_0;
    }

    public int getGewon3_2(Long id) {
        return klassementFacade.getGew3_2(id);
    }

    public void setGewon3_2(int gewon3_2) {
        this.gewon3_2 = gewon3_2;
    }

    public int getVer3_0(Long id) {
        return klassementFacade.getVer3_0(id);
    }

    public void setVer3_0(int ver3_0) {
        this.ver3_0 = ver3_0;
    }

    public int getVer3_2(Long id) {
        return klassementFacade.getVer3_2(id);
    }

    public void setVer3_2(int ver3_2) {
        this.ver3_2 = ver3_2;
    }

    public int getGewSet(Long id) {
        return klassementFacade.getGewSets(id);
    }

    public void setGewSet(int gewSet) {
        this.gewSet = gewSet;
    }

    public int getVerSet(Long id) {
        return klassementFacade.getVerSets(id);
    }

    public void setVerSet(int verSet) {
        this.verSet = verSet;
    }   

    public int getPunten(Long id) {
        return klassementFacade.getScore(id);
    }

    public void setPunten(int punten) {
        this.punten = punten;
    }
    
    
}

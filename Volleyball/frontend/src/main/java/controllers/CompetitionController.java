package controllers;

import com.realdolmen.maven.volleyballKlassement.dto.CompetitionDTO;
import com.realdolmen.maven.volleyballKlassement.facades.VolleyballFacade;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@RequestScoped
public class CompetitionController implements Serializable {

    @Inject
    private VolleyballFacade volleyballFacade;

    private CompetitionDTO competition;

    @PostConstruct
    public void init() {
       
    }
    
    public CompetitionDTO getCompetition() {
        return competition;
    }

    public void setCompetition(Long id) {
        this.competition = volleyballFacade.getCompById(id);
    }
    
    
}

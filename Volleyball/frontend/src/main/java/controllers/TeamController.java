package controllers;

import com.realdolmen.maven.volleyballKlassement.dto.TeamDTO;
import com.realdolmen.maven.volleyballKlassement.facades.VolleyballFacade;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@RequestScoped
public class TeamController implements Serializable{
    
    @Inject
    VolleyballFacade volleyballFacade;
    
    private TeamDTO team;
    
    @PostConstruct
    public void init(){
       
    }

    public TeamDTO getTeam() {
        return team;
    }

    public void setTeam(Long id) {
        this.team = volleyballFacade.getTeamById(id);
    } 
}

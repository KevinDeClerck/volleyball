/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rest.services;

import com.realdolmen.maven.volleyballKlassement.dto.ClubDTO;
import com.realdolmen.maven.volleyballKlassement.facades.VolleyballFacade;
import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author demun
 */
@Path("club")
public class ClubRestService {
    @Inject
    private VolleyballFacade volleyballFacade;
    
    @GET
    @Path("all")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findAllClubs(){
        List<ClubDTO> clubs = volleyballFacade.getAllClubs();
        return Response.status(Response.Status.OK)
                .entity(clubs)
                .build();
    }
    
    @GET
    @Path("{club_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findById(@PathParam("club_id")Long id){
        ClubDTO c = volleyballFacade.getClubById(id);
        if(c != null){
            return Response.status(Response.Status.OK)
                    .entity(c)
                    .build();
        }
        return Response.status(Response.Status.NOT_FOUND)
                .build();
    }
    
    
}

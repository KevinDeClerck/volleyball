/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rest.services;

import com.realdolmen.maven.volleyballKlassement.domain.Team;
import com.realdolmen.maven.volleyballKlassement.dto.TeamDTO;
import com.realdolmen.maven.volleyballKlassement.facades.VolleyballFacade;
import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author demun
 */
@Path("team")
public class TeamRestService {
    
    @Inject
    private VolleyballFacade volleyballFacade;
    
    @GET
    @Path("all")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findAllTeams(){
        List<TeamDTO> teams = volleyballFacade.getAllTeams();
        return Response.status(Response.Status.OK)
                .entity(teams)
                .build();
    }
     
    @GET
    @Path("{team_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findById(@PathParam("team_id")Long id){
        TeamDTO t = volleyballFacade.getTeamById(id);
        if(t != null){
            return Response.status(Response.Status.OK)
                    .entity(t)
                    .build();
        }
        return Response.status(Response.Status.NOT_FOUND)
                .build();
    }

}

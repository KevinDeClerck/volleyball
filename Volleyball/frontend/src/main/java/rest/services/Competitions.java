/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rest.services;

import com.realdolmen.maven.volleyballKlassement.dto.CompetitionDTO;
import java.util.List;

/**
 *
 * @author demun
 */
public class Competitions {
    private List<CompetitionDTO> competitions;
    
    public Competitions(List<CompetitionDTO> competitions){
        this.competitions = competitions;
    }
}

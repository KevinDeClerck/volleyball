/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rest.services;

import com.realdolmen.maven.volleyballKlassement.dto.GameDTO;
import com.realdolmen.maven.volleyballKlassement.dto.PlayerDTO;
import com.realdolmen.maven.volleyballKlassement.facades.VolleyballFacade;
import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author demun
 */
@Path("player")
public class PlayerRestService {
     @Inject
    private VolleyballFacade volleyballFacade;
     
     
     @GET
    @Path("all")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findAllPlayers(){
        List<PlayerDTO> players = volleyballFacade.getAllPlayers();
        return Response.status(Response.Status.OK)
                .entity(players)
                .build();
    }
     
    @GET
    @Path("{player_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findById(@PathParam("player_id")Long id){
        PlayerDTO p = volleyballFacade.getPlayerById(id);
        if(p != null){
            return Response.status(Response.Status.OK)
                    .entity(p)
                    .build();
        }
        return Response.status(Response.Status.NOT_FOUND)
                .build();
    }

     
     
}

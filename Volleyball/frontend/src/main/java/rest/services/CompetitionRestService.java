/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rest.services;

import com.realdolmen.maven.volleyballKlassement.domain.Competition;
import com.realdolmen.maven.volleyballKlassement.dto.CompetitionDTO;
import com.realdolmen.maven.volleyballKlassement.facades.VolleyballFacade;
import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author demun
 */
@Path("competition")
public class CompetitionRestService {
     @Inject
    private VolleyballFacade volleyballFacade;
     
     @GET
    @Path("all")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findAllCompetitions(){
        List<CompetitionDTO> competitions = volleyballFacade.getAllComps();
        return Response.status(Response.Status.OK)
                .entity(competitions)
                .build();
    }
    
    @GET
    @Path("{competition_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findById(@PathParam("competition_id")Long id){
        CompetitionDTO c = volleyballFacade.getCompById(id);
        if(c != null){
            return Response.status(Response.Status.OK)
                    .entity(c)
                    .build();
        }
        return Response.status(Response.Status.NOT_FOUND)
                .build();
    }
    
     
}

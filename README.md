# Volleyball

#REST service

url: api/competition/all

response:

competition: ObjectArray

ID:Long

naam:String

division:String

region:String

teamList: Object

    ID:Long

    naam:String

    playerList:Object

        ID:Long

            firstName:String

            lastName:String

            birthDate:String
    




*example

"competition" : [{
   
    ID:1000,
   
    "naam":"bv-1ste provinciale A",
   
    "division":"eerste provinciale",
   
    "region":"vlaams-brabant",
   
    "teamList":{"id":1000,"naam":"Lot",
   
    "playerList":{"id":1000,
   
    "firstName":"Jelter",
   
    "lastName":"Demunter",
   
    "birthDate":839282400000}
}]
... 


#find competition by id
url: api/competition/{id}

param:
    id: Long
    
response:


competition: ObjectArray

ID:Long

naam:String

division:String

region:String

teamList: Object

    ID:Long

    naam:String

    playerList:Object

        ID:Long

            firstName:String

            lastName:String

            birthDate:String
 


*example

"competition" : [{
   
    ID:1000,
   
    "naam":"bv-1ste provinciale A",
   
    "division":"eerste provinciale",
   
    "region":"vlaams-brabant",
   
    "teamList":{"id":1000,"naam":"Lot",
   
    "playerList":{"id":1000,
   
    "firstName":"Jelter",
   
    "lastName":"Demunter",
   
    "birthDate":839282400000}
}]
...   


    







